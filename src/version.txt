Exchange Enhancement
Chrome Extension for Microsoft Office Outlook Web Access 2003

VERSION HISTORY
___________________
v.1.1.0
    -Feature: Check for new mail every half minute in background
    -Feature: Favicon updates, if there is unread mail in the inbox

v.1.0.5:
    -Bugfix: Contacts were briefly shown, when redirecting. Blank page now.

v.1.0.4:
    -Bugfix: Sending a message redirected to Contacts. Crude fix, but working

v.1.0.3:
    -Bugfix: Autocomplete didn't initiate on first keypress after adding contact

v.1.0.2:
    -Feature: Put best matches first in sorted autocomplete list

v.1.0.1:
    - Bugfix: Missing semicolon after adding local contact email

v.1.0.0:
    Initial release
    - Feature: Autocomplete composer To/CC/BCC fields with local and added contacts.



