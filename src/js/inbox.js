var checkNewMail;   //Contains interval for mail checking

function Inbox()
{
    var xhr;            //Used to reset ajax requests

    var folderBar   = $('.tblFolderBar');
    var inboxTable  = $('.tblFolderBar').next();

    var searchInput = $('<input type="text" value="Søg" />');
    var toolBar     =  $('<div class="newToolBar"></div>');

    //Initialise variables
    var currentMailsPerPage;
    var contactData = [];

//Initialise new DOM elements
    var composerWrapper = $('<div id="composerWrapper"></div>');

    var outlookSettings = $('<iframe id="outlookSettings" width="100%" height="100%" style="visibility: visible;" src="' + optionsUrl + '" />');

    var inboxBtn = $('<div class="toolBarBtn"><span>indbakke</span><img src="' + chrome.extension.getURL('images/appbar.inbox.png') + '"/></div>');
    var newMailBtn = $('<div class="toolBarBtn"><span>ny mail</span><img src="' + chrome.extension.getURL('images/appbar.email.png') + '"/></div>');
    var contactsBtn = $('<div class="toolBarBtn"><span>ny kontakt</span><img src="' + chrome.extension.getURL('images/appbar.people.png') + '"/></div>');
    var savedMailsBtn = $('<div class="toolBarBtn"><span>gemt</span><img src="' + chrome.extension.getURL('images/appbar.cabinet.files.png') + '"/></div>');
    var sentMailsBtn = $('<div class="toolBarBtn"><span>sendt</span><img src="' + chrome.extension.getURL('images/appbar.inbox.out.png') + '"/></div>');

    var replyMailBtn = $('<div class="mtoolBarBtn"><img title="Svar" src="' + chrome.extension.getURL('images/appbar.reply.email.png') + '"/></div>');
    var replyAllBtn = $('<div class="mtoolBarBtn"><img title="Svar alle" src="' + chrome.extension.getURL('images/appbar.reply.people.png') + '"/></div>');
    var forwardBtn = $('<div class="mtoolBarBtn"><img title="Videresend" src="' + chrome.extension.getURL('images/appbar.arrow.right.png') + '"/></div>');
    var deleteBtn  = $('<div class="mtoolBarBtn"><img title="Slet" src="' + chrome.extension.getURL('images/appbar.delete.png') + '"/></div>');
    var saveBtn  = $('<div class="mtoolBarBtn"><img title="Gem" src="' + chrome.extension.getURL('images/appbar.save.png') + '"/></div>');


    folderBar.before(leftContentWrapper);
    folderBar.before(rightContentWrapper);
    rightContentWrapper.slideUp(0);
    rightContentWrapper.append(contentIframe);

    folderBar.detach();
    inboxTable.detach();

    leftContentWrapper.append(folderBar, inboxTable);

    $('table.trToolbar').replaceWith(toolBar);


    toolBar.append(inboxBtn, newMailBtn, contactsBtn, savedMailsBtn, sentMailsBtn, searchInput);

    $('.toolBarBtn').on('click', function()
    {
        $('.toolBarBtnClicked').removeClass('toolBarBtnClicked');
        $(this).addClass('toolBarBtnClicked');
    });

    inboxBtn.on('click', function(){
        leftContent(inboxUrl);
    });

    sentMailsBtn.on('click', function(){
        leftContent(sentMailsUrl);
    });

    savedMailsBtn.on('click', function(){
        leftContent(savedUrl);
    });

    newMailBtn.on('click', function(){
        rightContent(newMailUrl);
    });

    var preventDefault = true;

    searchInput.focus(function() {
        //Inject styles into the composer frame
        $('head').append('<link rel="stylesheet" href="' + chrome.extension.getURL('css/jquery-ui-1.10.3.custom.css') + '" type="text/css" />');

        //Prepare search

        $('body').append(outlookSettings);

        //Load autocomplete data from the hidden search field
        outlookSettings.bind('load', function()
        {
            stopCheckMail();
            currentMailsPerPage = outlookSettings.contents().find('select[name*="viewrowcount"]').val();
            outlookSettings.contents().find('select[name*="viewrowcount"]').val('100');
            SubmitWithCallback(outlookSettings.contents().find('form[name*="OptionsPageForm"]'), outlookSettings, function()
            {
                searchInput.autocomplete({
                    source: ["one", "two"]
                });

            });

            outlookSettings.unbind('load');
        });
    });
    searchInput.blur(function(){
        outlookSettings.attr('src', optionsUrl);
        outlookSettings.bind('load', function()
        {
            outlookSettings.contents().find('select[name*="viewrowcount"]').val(currentMailsPerPage);
            //outlookSettings.contents().find('form[name*="OptionsPageForm"]').submit();

            SubmitWithCallback(outlookSettings.contents().find('form[name*="OptionsPageForm"]'), outlookSettings, function()
            {
                outlookSettings.unbind('load');
                outlookSettings.remove();
            });
        });
        startCheckMail();

    });

    contentIframe.on('load', function(){
        var iframeContents = $(this).contents();

        iframeContents.find('head').append('<link rel="stylesheet" href="' + chrome.extension.getURL('css/eeStyle.css') + '" type="text/css" />');

        //These timeouts are to make sure the contentwrapper slides down, after the iframe has loaded
        setTimeout(function(){
            iframeContents.find('body').css('display', 'block');
            setTimeout(function(){
                rightContentWrapper.slideDown(300);
            }, 10);

        }, 10);


        $(this).height(this.contentWindow.document.body.scrollHeight + 'px');

        var isMailView = iframeContents.find('#idReadMessageHeaderTbl').length > 0;
        var isComposer = false;

        if(isMailView)
        {
            var base = iframeContents.find('base').attr('href');
            var mtoolBar = iframeContents.find('.trToolbar');

            //Remove dividers
            mtoolBar.find('img[src*="div.gif"]').parent().remove();

            mtoolBar.find('img[src*="icon-msg-reply.gif"]').replaceWith(replyMailBtn);
            replyMailBtn.parent().parent().next().remove();

            mtoolBar.find('img[src*="replyall.gif"]').replaceWith(replyAllBtn);
            replyAllBtn.parent().parent().next().remove();

            mtoolBar.find('img[src*="icon-msg-forward.gif"]').replaceWith(forwardBtn);
            forwardBtn.parent().parent().next().remove();

            //Remove the move icon
            mtoolBar.find('img[src*="moveto.gif"]').parent().parent().remove();


            mtoolBar.find('img[src*="prev.gif"]').parent().parent().remove();
            mtoolBar.find('img[src*="next.gif"]').parent().parent().next().remove();
            mtoolBar.find('img[src*="next.gif"]').parent().parent().remove();
            mtoolBar.find('img[src*="help.gif"]').parent().parent().next().remove();
            mtoolBar.find('img[src*="help.gif"]').parent().parent().remove();


            mtoolBar.find('img[src*="copyto.gif"]').replaceWith(saveBtn);
            saveBtn.parent().parent().next().remove();


            mtoolBar.find('.mtoolBarBtn').off('click');
            mtoolBar.find('.mtoolBarBtn').on('click', function(e)
            {
                var that = this;
                if(preventDefault) {

                    e.preventDefault();

                    preventDefault = false;
                    rightContentWrapper.slideUp(300, function() {
                        $(that).click();
                        preventDefault = true;
                    });
                }

            });


        }
        else if(isComposer)
        {
            var msgTo =  composer.contents().find('.tblForm').find("input[name*='MsgTo']");
            $(that).is(":visible") ? msgTo.focus() : msgTo.blur();
            //Wrap current DOM elements
            var composerSendBtn = composer.contents().find('a[href*="cmdSend"]');
            //Define functionalities
            composerSendBtn.on('click', function() {
                slideContents.hide();
                slideContents.html('');

                startCheckMail();
            });
            //Place DOM elements
            composer.contents().find('head').append('<link rel="stylesheet" href="' + chrome.extension.getURL('css/jquery-ui-1.10.3.custom.css') + '" type="text/css" />');

            var msgInputs       = composer.contents().find("input[name*='Msg']");

            msgInputs.autocomplete({
                source: function(request, response){
                    //Remove added mails from the search terms
                    var term = request.term.split('; ').pop();

                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex( term ), "i" );
                    response( $.grep( contactData.sort(dynamicSort("label", term)), function( value )
                    {
                        //Make sure all fields are searched
                        value = value.label + value.value + value;
                        return matcher.test( value );
                    }) );
                },
                focus: function(event) {
                    event.preventDefault();
                },
                select: function( event, ui ) {
                    event.preventDefault();

                    //Append new mails to the pre-existing ones
                    var parts = $(this).val().split(';');
                    var keep;

                    parts.pop();

                    if(parts.length == 0) {
                        keep = parts.join('');
                    }
                    else {
                        keep = parts.join(';') + '; ';
                    }

                    $(this).val(keep + ui.item.value);
                }
            });
        }

    });

    createContactList();
    startCheckMail();
    replaceInboxLinks();

    var leftContent = function(url, callback)
    {
        if(!url)
        {
            url = inboxUrl;
        }

        if(xhr && xhr.readyState != 4){
            xhr.abort();
        }


        url = cleanUrl(url);

        xhr =  $.ajax({
            url: url,
            dataType: 'html',
            cache: false,
            success: function(response)
            {
                stopCheckMail();

                if(folderBar.find('b:contains("Indbakke")'))
                {
                    startCheckMail();
                }

                inboxTable.hide().html($(response).find('.tblFolderBar').next()).fadeIn(300);
                folderBar.html($(response).find('.tblFolderBar'));

                replaceInboxLinks();
            },

            complete: function(){
                //If there is a message unread image showing, change the fav-icon
                if($('img[src*="msg-unread.gif"]').length > 0)
                {
                    $("#favicon").attr("href",chrome.extension.getURL('images/newmailfav.png'));
                }
                else
                {
                    $("#favicon").attr("href",chrome.extension.getURL('images/mailfav.png'));
                }

                if($.isFunction(callback)) {
                    callback();
                }
            },
            error: function(xhr, err) {
                var responseTitle= $(xhr.responseText).filter('title').get(0);
                console.log(timeStamp() + ' :: ' + $(responseTitle).text() + "\n" + formatErrorMessage(xhr, err) );
            }
        });
    };

    var rightContent = function (url)
    {
        rightContentWrapper.slideUp(300, function() {
            contentIframe.attr('src', cleanUrl(url));
        });
    };

    return {
        leftContent: leftContent,
        rightContent: rightContent
    };

    function cleanUrl(url)
    {
        url = decodeURI(url);
        return encodeURI(url).replace(/'/g,"%27").replace(/"/g,"%22");
    }

    function startCheckMail() {
        stopCheckMail();
        checkNewMail = window.setInterval(leftContent, 60000);
    }

    function stopCheckMail() {
        clearInterval(checkNewMail);
    }

    function replaceInboxLinks() {
        inboxTable = $('.tblFolderBar').next();

        var inboxLines = inboxTable.find('.List').parent();

        inboxLines.each(function(){
            var name    = $('<div class="mailName">' + $(this).find('td:nth-child(6) font>a>font').html() + '</div>');
            var title   = $('<div class="mailTitle">' + $(this).find('td:nth-child(7) font>a>font').html() + '</div>');
            var date    = $('<div class="mailDate">' + $(this).find('td:nth-child(8) font>a>font').html() + '</div>');
            var link    = $(this).find('td:first-child').find('input').val();



            $(this).find('td').remove();

            var combined = $('<td class="mailLine" colspan="9"></td>');

            $(this).append(combined);

            combined.append(name, title, date);
            combined.attr('data-link', link);

        });

        $('.mailLine').on('click', function(){
            rightContent(baseURL + $(this).attr('data-link') + '?Cmd=open');
        });
    }

    function createContactList()
    {
        //Create contacts list
        var localContacts = $('<iframe id="localContacts" style="visibility: hidden;" src="' + findContactsUrl +'" />');
        var addedContacts = $('<iframe id="addedContacts" style="visibility: hidden;" src="' + contactsUrl + '" />');

        $('body').append(localContacts, addedContacts);

        //Load autocomplete data from the hidden search fields
        addedContacts.bind('load', function()
        {
            var contents          = $(this).contents();

            //The tags have no ID's, which is the reason for this convoluted approach
            var parentTable     = contents.find('table.tblFolderBar').next().children("tbody").children('tr').children('td').children('table');
            var countContacts   = parentTable.children('tbody').children('tr').length / 3; //Each item has 3 tr's

            var row = 1;
            var searchArea = parentTable.children('tbody');
            for(var i= 0; i < countContacts; i++)
            {
                row = row + i;
                var labelName   = searchArea.children('tr:nth-child(' + row + ')').find('font').children('b').html().split(',').reverse().join(' ').trim();
                var mail        = searchArea.children('tr:nth-child(' + (row + 1) + ')').children('td:nth-child(2)').find('font').html().replace(/&nbsp;/gi,'').trim() + '; ';

                contactData.push({label: labelName, value: mail});
                row = row +2;
            }

            addedContacts.remove();
        });

        //Load autocomplete data from the hidden search field
        localContacts.bind('load', function()
        {
            var contents        = $(this).contents();

            //The tags have no ID's, which is the reason for this convoluted approach
            var countContacts   = contents.find('.trDlgDesc').next().parent().children("tr").length;

            var searchArea = contents.find('.trDlgDesc').next().parent();

            //The contacts info start at the third tr tag
            for(var i= 3; i < countContacts; i++)
            {
                var labelName = searchArea.children("tr:nth-child("+i+")").children("td").children("font").html();
                var mail      = searchArea.children("tr:nth-child("+i+")").children("td").children("input").val();
                if(labelName && mail) {
                    contactData.push({label: labelName + ' @' + siteName, value: mail + '; '});
                }
            }

            localContacts.remove();

            //LAST PAGE LOAD
            console.timeEnd('Full page');
            $('body').fadeIn(200);
        });
    }

}




