/**
 * Created with JetBrains WebStorm.
 * User: Christian
 * Date: 20/10/13
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */

function onBeforeNaviCallback(detail){

    if(detail.frameId > 0 && findString(detail.url,'Indbakke/?Cmd=contents')) {

        chrome.tabs.update(detail.tabId, {
            url: detail.url
        });
    }
}

function findString(str, search)
{
    return str.indexOf(search) !== -1;
}

chrome.webNavigation.onBeforeNavigate.addListener(onBeforeNaviCallback);



