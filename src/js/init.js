console.time('Page load');

var siteName    = generateSiteName();
var baseURL     = findBaseUrl();

//Define common urls
var sentMailsUrl    = baseURL + 'Sendt%20post/?Cmd=contents';
var inboxDefaultUrl = baseURL + 'Indbakke/?Cmd=contents';
var inboxUrl        = baseURL + 'Indbakke/?Cmd=contents&View=Meddelelser';
var optionsUrl      = baseURL + '?Cmd=options';
var savedUrl        = baseURL + 'Opgaver/?Cmd=contents';
var findContactsUrl = baseURL + '?Cmd=galfind&US=M&DN=+';
var contactsUrl     = baseURL + 'Kontaktpersoner/?Cmd=contents';
var newMailUrl      = baseURL + 'Kladder/?Cmd=new';

//Define mail specific urls
var replyUrl        = '?Cmd=reply';
var replyAllUrl     = '?Cmd=replyall';
var forwardUrl      = '?Cmd=forward';

//Define new content wrappers
var rightContentWrapper  = $('<div id="rightContentWrapper"></div>');
var leftContentWrapper    = $('<div id="leftContentWrapper"></div>');
var contentIframe   = $('<iframe id="contentIframe" scrolling="no" src="" />')

$(document).ready(function()
{
    //Define booleans and check which page is visited
    var isInboxPage         = $('form').attr('action') && findString($('form').attr('action'), 'Indbakke');

    appendFavIcon();
    appendTitle('Exchange Enhancement 1.1.0');

    if(isInboxPage)
    {
        var inbox = Inbox();

        inbox.leftContent(inboxUrl);

        reconstructInboxPageDOM();

    }
    else
    {
        $('body').show();
    }
});