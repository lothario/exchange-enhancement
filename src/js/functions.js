/**
 * Created with JetBrains WebStorm.
 * User: Christian
 * Date: 03/11/13
 * Time: 21:56
 * To change this template use File | Settings | File Templates.
 */

/**
 * Returns the string before the top level domain (f.ex. google from www.google.com)
 * @returns {string}
 */
function generateSiteName()
{
    var hostName = document.location.hostname;
    var hostNameParts = hostName.split('.');
    hostNameParts.pop();
    return hostNameParts.pop();
}

/**
 * Returns the working dir of the exchange web page
 * @returns {string}
 */
function findBaseUrl()
{
    //Extract baseurl from url
    var fullURL = document.URL;
    var baseURLParts = fullURL.split('/');
    baseURLParts.pop();
    baseURLParts.pop();
    return baseURLParts.join('/') + '/';
}

/**
 *
 */
function appendFavIcon()
{
    $('head').append('<link id="favicon" rel="shortcut icon" type="image/png" href="' + chrome.extension.getURL('images/mailfav.png') + '" />');
}

/**
 *
 * @param title
 */
function appendTitle(title)
{
    $('title').length > 0 ? $('title').html(title) : $('head').append('<title>' + title + '</title>');
}

function findString(str, search)
{
    return str.indexOf(search) !== -1;
}

function dynamicSort(property, term) {

    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;

        var aComp = a[property].toLowerCase();
        var bComp = b[property].toLowerCase();
        term = term.toLowerCase();

        //Order items, that starts with the searchterm higher than others
        if(aComp.indexOf(term) < bComp.indexOf(term))
        {
            //Lower is better, but if the search term doesn't appear in the string, reverse the result
            result = aComp.indexOf(term) !== -1? -1: 1;
        }
        else if(aComp.indexOf(term) > bComp.indexOf(term))
        {
            //Lower is better, but if the search term doesn't appear in the string, reverse the result
            result = bComp.indexOf(term) !== -1 ? 1:-1;
        }

        return result;
    }
}

function timeStamp() {
    var now = new Date();
    var outStr = ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2) + ':' + ('0'+ now.getSeconds()).slice(-2);
    return outStr;
}

function SubmitWithCallback(form, frame, successFunction) {
    var callback = function () {
        if(successFunction) {
            successFunction();
        }

        frame.unbind('load', callback);
    };

    frame.bind('load', callback);
    form.submit();
}



function formatErrorMessage(jqXHR, exception) {

    if (jqXHR.status === 0) {
        return ('Not connected.\nPlease verify your network connection.');
    } else if (jqXHR.status == 404) {
        return ('The requested page not found. [404]');
    } else if (jqXHR.status == 500) {
        return ('Internal Server Error [500].');
    } else if (exception === 'parsererror') {
        return ('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        return ('Time out error.');
    } else if (exception === 'abort') {
        return ('Ajax request aborted.');
    } else {
        return ('Uncaught Error.\n' + jqXHR.responseText);
    }
}





